import 'package:flutter/material.dart';
import 'body_answers.dart';
import 'body_questions.dart';

void main() {
//the root of the app
    runApp(App());
}

class App extends StatefulWidget {
@override
_AppState createState() => _AppState();
}

class _AppState extends State<App> {
    late int questionIdx = 0;
    bool showAnswers = false;

    final questions = [
        {
            'question': 'What is the nature of your business needs?',
            'options': ['Time Tracking', 'Asset Management', 'Issue Tracking'],
        },
        {
            'question': 'What is the expected size of the user base?',
            'options': ['Less than 1,000', 'Less than 10,000', 'More than 10,000'],
        },
        {
            'question': 'In which region would the majority of the user base be?',
            'options': ['Asia', 'Europe', 'Americas', 'Africa', 'Middle East'],
        },
        {
            'question': 'What is the expected project duration?',
            'options': ['Less than 3 months','3-6 months','6-9 months','9-12 months','More than 12 months',],
        },
    ];

    var answers =[];

void nextQuestion(String? answer) {
    print(answer);
    answers.add({
        'question':questions[questionIdx]['question'],
        'answer': (answer == null) ? '': answer
    });

    if(questionIdx < questions.length - 1){
        setState(() => {questionIdx++});
    }else{
        setState(() =>showAnswers = true);
    }
}

@override
Widget build(BuildContext context) {
//The scaffold provides basic design layout structure
//The scaffold can be given UI elementes such as app bar.
var bodyQuestions =  BodyQuestions(questions: questions, questionIdx: questionIdx, nextQuestion: nextQuestion);
var bodyAnswers = BodyAnswers(answers: answers);

Scaffold homepage = Scaffold(
    appBar: AppBar(
    title: Text('Homepage',textAlign: TextAlign.center,),),
    body: (showAnswers) ? bodyAnswers : bodyQuestions,
);
return MaterialApp(
    home: homepage,
);
}
}

//The invinsible widgets are Containers and Scaffold
//The visible widgets are Appbar and Text

//To specify margin or padding spacing, we can use EdgeInsets.
//The values for spacing are in multioples of 8 (eg. 16,24)
//To add spacing on all sides, use EdgeInsets.all()
//To add spacing on certain sides, use EdgeInsets.only(direction: value)

//In flutter width = double.infinity is equivalent of with == 100%
//to put colors on a container, the decoration: BoxDecorationColor: Colors.green can change

//In a Column widget, the main axis alignment is vertical (or from top to bot)
//to change the horizontal alignment of widgets in a col, we must use the crossAxisAlignment
//For example, if we want to place column widgets to the horizontal left, we use crossAxisAlignment.start
//In the context of the col, we can consider the column as vertical, and its cross axis as horizontal